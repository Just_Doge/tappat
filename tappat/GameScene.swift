//
//  GameScene.swift
//  tappat
//
//  Created by Geddy on 23/4/17.
//  Copyright © 2017 Smilez. All rights reserved.
//

import SpriteKit
import GameplayKit

var square = SKSpriteNode()
var darkSquare = SKSpriteNode()
var lblMain = SKLabelNode()
var lblScore = SKLabelNode()

var offBlackColor = UIColor(red: 0.2, green: 0.2, blue: 0.2, alpha: 1.0)
var offWhiteColor = UIColor(red: 0.98, green: 0.98, blue: 0.98, alpha: 1.0)
var BGP = UIColor(red: 0.7, green: 0.0, blue: 0.7, alpha: 1.0)
var BGB = UIColor(red: 0.2, green: 0.5, blue: 0.9, alpha: 1.0)

var colorChoice = 0

var touchLocation = CGPoint()
var touchedNode = SKNode()

var introMessages = ["Ready", "Set", "Tap!"]

var randomSquareChoice = arc4random_uniform(2)
var squareList = [square.name, darkSquare.name]
var timerCounter = -1
var canTap = false
var score = 0



class GameScene: SKScene {
    
    
    
    override func didMove(to view: SKView) {
        chooseBGColor()
        resetGame()
        spawnLblMain()
        spawnLblScore()
        
        startGameTimer()
        
    }
    func chooseBGColor() {
        switch colorChoice {
        case 0:
            self.backgroundColor = BGB
        case 1:
            self.backgroundColor = BGP
        case 2:
            colorChoice = 0
        default:
            self.backgroundColor = UIColor.black
        }
        
        colorChoice += 1
    }
    
    func resetGame() {
        randomSquareChoice = arc4random_uniform(2)
        print(randomSquareChoice)
        timerCounter = -1
        canTap = false
        score = 0
    }
    
    func chooseSquare() -> String {
        var correctSquare = squareList[Int(randomSquareChoice)]
        return correctSquare!
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        
        
        for t in touches {
            touchLocation = t.location(in: self)
            touchedNode = atPoint(touchLocation)
            if canTap {
                touchedNodeLogic()
            }
        }
    }
    
    func spawnSquare() {
        square = SKSpriteNode(color: offWhiteColor, size: CGSize(width: 300, height: 300))
        square.position = CGPoint(x: self.frame.midX, y: self.frame.midY - 100)
        
        square.name = "squareName"
        self.addChild(square)
    }
    
    func spawnDarkSquare() {
        darkSquare = SKSpriteNode(color: offBlackColor, size: CGSize(width: 300, height: 300))
        darkSquare.position = CGPoint(x: self.frame.midX, y: self.frame.midY - 400)
        darkSquare.name = "darkSquareName"
        self.addChild(darkSquare)
    }
    
    func spawnLblMain() {
        lblMain = SKLabelNode(fontNamed: "Futura")
        lblMain.fontColor = offWhiteColor
        lblMain.fontSize = 180
        lblMain.position = CGPoint(x: self.frame.midX, y: self.frame.midY + 470)
        lblMain.text = "\(introMessages[0])"
        
        self.addChild(lblMain)
        
    }
    
    func spawnLblScore() {
        lblScore = SKLabelNode(fontNamed: "Futura")
        lblScore.fontSize = 90
        lblScore.fontColor = offWhiteColor
        lblScore.position = CGPoint(x: self.frame.midX, y: self.frame.minY + 100)
        lblScore.text = "Score: \(score)"
        self.addChild(lblScore)
        
    }
    
    func startGameTimer() {
        let wait = SKAction.wait(forDuration: 1.0)
        let countDownTimer = SKAction.run {
            timerCounter += 1
            
            switch timerCounter {
            case -1:
                lblMain.text = "Ready"
            case 0:
                lblMain.text = "\(introMessages[timerCounter])"
            case 1:
                lblMain.text = "\(introMessages[timerCounter])"
            case 2:
                lblMain.text = "\(introMessages[timerCounter])"
                self.spawnSquare()
                self.spawnDarkSquare()
                canTap = true
            case 11:
                self.gameOverLogic()
            default:
                print("Continued")
            }
        }
        
        let sequence = SKAction.sequence([wait, countDownTimer])
        self.run(SKAction.repeatForever(sequence))
    }
    
    func touchedNodeLogic() {
        if let nodeName = touchedNode.name {
            if nodeName == chooseSquare() {
                
                score += 1
                updateScore()
                randomSquareChoice = arc4random_uniform(2)
                print(randomSquareChoice)
            } else {
                gameOverLogic()
            }
        }
    }
    
    func updateScore() {
        lblScore.text = "Score: \(score)"
        print("tapped")
    }
    
    func gameOverLogic() {
        square.removeFromParent()
        darkSquare.removeFromParent()
        canTap = false
        
        lblMain.text = "Done!"
        
        let theGameScene = GameScene(fileNamed: "GameScene")
        let theTransition = SKTransition.fade(withDuration: 0.8)
        
        theGameScene?.scaleMode = .aspectFill
        
        let wait = SKAction.wait(forDuration: 3.0)
        let runTransition = SKAction.run {
            self.scene?.view?.presentScene(theGameScene!, transition: theTransition)
        }
        
        let sequence = SKAction.sequence([wait, runTransition])
        self.run(SKAction.repeat(sequence, count: 1))
    }
    
    override func update(_ currentTime: TimeInterval) {
        // Called before each frame is rendered
    }
}
